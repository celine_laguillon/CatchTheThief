#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* ****************************************************************************************** */
/* *************************************** Explications ************************************* */
/* ****************************************************************************************** */

/* Stratégie adoptée */

	/* Principe général */
		
		// Les agents de brigade rapide se déplacent dès le début à l'étage 2 pour prendre de l'avance sur les agents concurrents.
		// Chaque agent parcourt les étages à partir de son escalier de départ.
		// L'agent 1, dans l'escalier de gauche, se spécialise service de surveillance. Il s'arrête à l'étage lorsqu'il a repéré au moins un voleur. Ainsi, le voleur ne peut plus changer d'étage de ce côté.
		// L'agent 2, dans l'escalier de droite, fouille les chambres d'un étage lorsque son collègue y a repéré des voleurs. De ce fait, les voleurs initialement repérés sont pris au piège.
		
		// Une fois arrivé en haut et qu'il reste des voleurs, l'agent 1 se spécialise brigade rapide. Les deux agents redescendent à l'étage 1. Puis, ils reprennent les recherches, jusqu'à avoir capturé tous les voleurs.
		
	/* Cas particuliers */
		
		// Lorsque l'agent 1 accède au dernier étage abritant des voleurs, il parcourt cet étage depuis la gauche, tandis que l'agent 2 le parcourt depuis la droite.

		// Si dans tout l'hôtel, les chambres aux extrêmités d'au moins un étage sur deux sont scellées, les voleurs sont contraints de rester à l'étage où ils se trouvent.
		// Dans ce cas, l'agent 1 finit de repérer tous les étages avec des voleurs, tandis que l'agent 2 fouille les étages avec au moins un voleur. Puis, l'agent 1 se spécialise brigade rapide et a le même comportement que l'agent 2 (il fouille les chambres des étages avec voleur), à un étage différent sauf si tous les voleurs restants sont répartis sur un étage.
		
	/* Risques */

		/* Risque principal */
		
			// Le risque principal est que tous les voleurs soient répartis sur les deux premiers étages.

			// Probabilité qu'un voleur soit sur un des deux premiers étages = 2 / (NOMBRE_ETAGES)
			// Pour qu'une partie soit perdue, il faut que (la moitié des voleurs + 1) (= E(NOMBRE_VOLEURS_PAR_JOUEUR / 2) + 1) soient sur les deux premiers étages.

			// La probabilité que la moitié des voleurs soit sur les deux premiers étages est de : 
			//		2 / (NOMBRE_ETAGES^(E(NOMBRE_VOLEURS / 2) + 1))
			// Or, pour chaque joueur, il y a deux voleurs : NOMBRE_VOLEURS / 2 = (NJ * 2) / 2 = NJ
			// Donc, dans le cas de la consigne, la probabilité est de : 
			// 		2 / (10^(NJ + 1)) = 1 / (5^(NJ + 1))
			// La probabilité que ce soit le cas est faible, donc le risque est accepté.
		
		/* Risque secondaire */
		
			// Le risque secondaire est que les voleurs soient répartis dans les chambres les plus à gauche de l'étage, c'est-à-dire les chambres les plus éloignées de l'agent 2.
			// Dans ce cas là, les voleurs risquent d'être capturés par des agents concurrents, avant que l'agent 2 ne les atteignent.
			
	/* Améliorations provisionnées */
	
		// Le nombre de joueurs peut être changé, grâce aux constantes.
		// Des fonctions pour des spécialisations actuellement non utilisées ont été implémenté (peloton de sûreté), pour permettre un changement de stratégie.

/* ****************************************************************************************** */
/* ********************************* Définition des constantes ****************************** */
/* ****************************************************************************************** */

// constantes d'exécution
#define NOMBRE_TOURS_MAX 300
#define TAILLE_BUFFER 128

// dimensions de l'hôtel
#define NOMBRE_COLONNES 10
#define NOMBRE_ETAGES 10

// première chambre de chaque côté
#define COTE_GAUCHE 1
#define COTE_DROIT NOMBRE_COLONNES - 2

// étage de départ de la fouille des étages par les agents
#define ETAGE_DEPART 2

// caractéristiques du jeu
#define NOMBRE_AGENTS_PAR_JOUEUR 2
#define NOMBRE_VOLEURS_PAR_JOUEUR 2

// spécialisations des agents
#define EN_COURS_DE_SPECIALISATION 0
#define BRIGADE_RAPIDE 1
#define PELOTON_DE_SURETE 2
#define SERVICE_DE_SURVEILLANCE 3
#define POLICE_SCIENTIFIQUE 4

/* ****************************************************************************************** */
/* ***************************** Définition des types de structure ************************** */
/* ****************************************************************************************** */

typedef struct{
	int J; // numéro du joueur
	int C; // nombre de points obtenus par le joueur dans la partie
	int END; // 1 si le joueur a perdu, 0 sinon (en réalité : 1 pendant la partie)
}Joueur;

typedef struct{
	int ID; // numéro de l'agent
	int J; // numéro du joueur propriétaire
	int F; // étage où se trouve l'agent
	int C; // colonne où se trouve l'agent
	int S; // specialisation de l'agent : [0, 4]
	// si l'agent est en cours de spécialisation, S = 0

	// Pour les agents perso
	int montee; // si l'agent parcourt l'hôtel en montant ou en descendant
	int cote; // colonne des chambres à côté de l'escalier de départ de l'agent
	int decal; // sens de parcours de l'étage (-1 s'il parcourt les chambres de la droite vers la gauche, 1 sinon)
	int etageAViser; // un étage où il y a au moins un voleur
	int caseAFouiller; // une chambre à fouiller, qui est à la fois non scellée et non déjà fouillée par un de mes agents
	int agentSURV; // si l'agent est spécialisé agent de surveillance et qu'il se spécialise brigade rapide pour se déplacer plus rapidement
}Agent;

typedef struct{
	int C; // numéro de colonne
	int scelle; // 1 si la chambre est scellée, 0 sinon
	int fouille; // 1 si la chambre a déjà été fouillé, 0 sinon
	int agentSurPlace; // 1 si un agent est devant ou dans la chambre, 0 sinon
	int voleurSurPlace; // nombre de voleurs dans la chambre (variable utile pour les agents spécialisés police scientifique, non utilisée ici)
	int agentSURVEscalier; // si un agent spécialisé Service de Surveillance est dans un escalier
}Case;

typedef struct{
	Case cases[NOMBRE_COLONNES]; // chambres d'un étage
	int NV; // nombre de voleurs à l'étage
	int verifFinie; // indique si l'agent de surveillance a fini de tout vérfier 
}Etage;

// informations générales sur l'hôtel
typedef struct{
	// STRUCTURE	
	Etage etages[NOMBRE_ETAGES]; // étages de l'hôtel
	int NS; // nombre de chambre(s) scellée(s)

	// JOUEURS
	int NJ; // nombre de joueurs
	Joueur* joueurs; // liste des joueurs
	int nombreJoueursEnJeu; // nombre de joueurs encore en jeu

	// AGENTS
	int NA; // nombre d'agents
	Agent* agents; // liste des agents
	int SURV; // nombre d'agents spécialisés Service de Surveillance dans un escalier
	int verifTotaleFinie; // si tous les étages ont été surveillé
	int etagesScelles; // si au moins un étage sur deux est scellé

	// VOLEURS
	int NVA; // nombre de voleur(s) attrapé(s) au tour précédent
	int nombreVoleursEnJeu; // nombre de voleurs encore en jeu

	// INFORMATIONS PERSONNELLES
	int JPerso; // numéro de mon joueur
	int agentsPerso[NOMBRE_AGENTS_PAR_JOUEUR]; // tableau avec les ID des agents contrôlés par mon joueur
	
	int compteurDeTours; // compteur de tour
}Hotel;

/* ****************************************************************************************** */
/* ********************************** Prototypes des fonctions ****************************** */
/* ****************************************************************************************** */

// Fonctions appelées dans le main
void initInfoHotel(Hotel*);
void recuperationDesInformations(Hotel*);
char* analyseDesInformations(Hotel*);
void envoiDesCommandes(char*);
void finDePartie(Hotel*);

// Fonctions intermédiaires : actions et déplacement des agents
char* actionsBrigadeRapideAccesDepart(Hotel*, Agent*);
char* actionsBrigadeRapideSpecialisationSURV(Agent*);
char* actionsBrigadeRapide(Hotel*, Etage*, Agent*);
char* actionsBrigadeRapideAgentSURV(Hotel*, Agent*);

char* actionsPelotonDeSurete(Hotel*, Etage*, Agent*);

char* actionsServiceDeSurveillance(Hotel*, Etage*, Agent*);
char* deplacementServiceDeSurveillance(Hotel*, Agent*);
char* deplacementServiceDeSurveillanceDernierEtage(Etage*, Agent*);

char* actionsPoliceScientifique();

// Fonctions de bas niveau : tests et recherche
int testDeplacementAgent(int, int);
int testRecherche(int);
int testEscalier(int);

int testEtagesScelles(Hotel*);
void rechercheEtageAViser(Hotel*, Etage*, Agent*);
void remiseAZeroFouilleEtage(Etage*);
int testCaseAFouiller(Agent*, Etage*);

int getIDAutreAgent(int);

/* ****************************************************************************************** */
/* *************************************** Fonction main ************************************ */
/* ****************************************************************************************** */

int main(void){	
	char* sortieStandard;

	Hotel* hotel = (Hotel*) malloc(sizeof(Hotel)); // définition de l'hôtel
	
	initInfoHotel(hotel);

	// boucle principale	
	do{
		recuperationDesInformations(hotel); // reçoit et organise les informations dans la structure Hotel
		
		sortieStandard = analyseDesInformations(hotel); // définition de la stratégie et des actions de chaque agent

		envoiDesCommandes(sortieStandard); // envoi des sorties au serveur

		hotel->compteurDeTours++; // incrémentation du nombre de tours
		
	}while(hotel->joueurs[hotel->JPerso].END && hotel->compteurDeTours < NOMBRE_TOURS_MAX);
	
	finDePartie(hotel);
	
	return 0;
}

/* ****************************************************************************************** */
/* ********************************** Fonction initInfoHotel ******************************** */
/* ****************************************************************************************** */
/* La fonction initialise les champs de la structure Hotel. */

/* Prend en paramètres le pointeur de la structure de jeu Hotel. */
void initInfoHotel(Hotel* hotel){
	// déclaration des variables de boucle
	int indiceAgent, etage, colonne;
	
	hotel->compteurDeTours = 0; // compteur de tours

	// informations reçues avant le premier tour
	scanf("%d %d", &hotel->NJ, &hotel->JPerso);

	hotel->joueurs = (Joueur*) malloc(hotel->NJ * sizeof(Joueur));

	// définition des structures Agent
	hotel->NA = NOMBRE_AGENTS_PAR_JOUEUR * hotel->NJ;
	hotel->agents = (Agent*) malloc(hotel->NA * sizeof(Agent));

	for(indiceAgent = 0 ; indiceAgent < NOMBRE_AGENTS_PAR_JOUEUR ; indiceAgent++){
		hotel->agentsPerso[indiceAgent] = NOMBRE_AGENTS_PAR_JOUEUR * hotel->JPerso + indiceAgent;
		hotel->agents[hotel->agentsPerso[indiceAgent]].montee = 1;
		
		if(indiceAgent == 0){ // pour l'agent qui commence à la colonne 0
			hotel->agents[hotel->agentsPerso[indiceAgent]].decal = 1;
			hotel->agents[hotel->agentsPerso[indiceAgent]].cote = COTE_GAUCHE;
		}
		
		else{ // pour l'agent qui commence dans l'autre escalier
			hotel->agents[hotel->agentsPerso[indiceAgent]].decal = -1;
			hotel->agents[hotel->agentsPerso[indiceAgent]].cote = COTE_DROIT;
		}
	}
	
	hotel->nombreVoleursEnJeu = NOMBRE_VOLEURS_PAR_JOUEUR * hotel->NJ;
	
	// initialisation du booléen de vérification de fouille des chambres
	hotel->verifTotaleFinie = 0;
	
	for(etage = 0 ; etage < NOMBRE_ETAGES ; etage++){
		hotel->etages[etage].verifFinie = 0;
		for(colonne = 1 ; colonne < (NOMBRE_COLONNES - 1) ; colonne++){
			hotel->etages[etage].cases[colonne].fouille = 0;
		}
		
		hotel->etages[etage].cases[0].fouille = 1;
		hotel->etages[etage].cases[NOMBRE_COLONNES - 1].fouille = 1;
	}
	
	hotel->etagesScelles = 0;
}

/* ****************************************************************************************** */
/* *************************** Fonction recuperationDesInformations ************************* */
/* ****************************************************************************************** */
/* Fonction qui récupère les entrées standards envoyées à chaque tour. */

/* Prend en entrée l'adresse de la structure Hotel et le compteur de tours. */
/* Renvoie un compte rendu d'exécution : 0 si tout va bien, -1 sinon. */
void recuperationDesInformations(Hotel* hotel){
	int F, C, indiceF, indiceC, indiceNS; // variables pour les étages et les colonnes
	int indiceID, indiceAgentPerso; // variables pour les agents
	int nombreJoueursEnJeu = 0, indiceJ; // variables pour les joueurs
	int indiceVoleurAttrape, E, tempNV, indiceSURV; // variables pour les voleurs
	int NI, indiceNI; // informations envoyées par la police scientifique
	
	// mise à 0 des agents au début de chaque tour
	for(indiceF = 0 ; indiceF < NOMBRE_COLONNES ; indiceF++){
		for(indiceC = 0 ; indiceC < NOMBRE_ETAGES ; indiceC++){
			hotel->etages[indiceF].cases[indiceC].agentSurPlace = 0;
		}
	}

	// informations sur les joueurs : J l'id du joueur, C son compteur de points, END s'il est hors jeu (1 sinon)
	for(indiceJ = 0 ; indiceJ < hotel->NJ ; indiceJ++){
		scanf("%d %d %d", &hotel->joueurs[indiceJ].J, &hotel->joueurs[indiceJ].C, &hotel->joueurs[indiceJ].END);

		if(hotel->joueurs[indiceJ].END){ // si le joueur est encore en jeu
			nombreJoueursEnJeu++;
		}
	}

	// nombre d'agents en jeu
	scanf("%d", &hotel->NA);

	// informations sur les agents
	indiceAgentPerso = 0;
	for(indiceID = 0 ; indiceID < hotel->NA ; indiceID++){
		scanf("%d %d %d %d %d", &hotel->agents[indiceID].ID, &hotel->agents[indiceID].J, &hotel->agents[indiceID].F, &hotel->agents[indiceID].C, &hotel->agents[indiceID].S);
		
		if(hotel->agents[indiceID].J == hotel->JPerso){ // mise à jour des indices des agents du joueur
			hotel->agentsPerso[indiceAgentPerso] = indiceID;
			indiceAgentPerso++;
		}

		else{ // si ce n'est pas un agent du joueur
			// mise à jour des cases où sont des agents 
			hotel->etages[hotel->agents[indiceID].F].cases[hotel->agents[indiceID].C].agentSurPlace = 1;
		}
	}

	// nombre de voleurs attrapés au tour précédent
	scanf("%d", &hotel->NVA);
	hotel->nombreVoleursEnJeu -= hotel->NVA;
	
	// informations sur la capture des voleurs
	for(indiceVoleurAttrape = 0 ; indiceVoleurAttrape < hotel->NVA ; indiceVoleurAttrape++){
		scanf("%d %d", &F, &C); // récupération des coordonnées de la chambre

		if(hotel->etages[F].cases[C].voleurSurPlace > 0){
			hotel->etages[F].cases[C].voleurSurPlace--; // décrémentation du nombre de voleur dans la chambre s'il a été attrapé
		}

		if(hotel->etages[F].NV > 0){
			hotel->etages[F].NV--; // decrémentation du nombre de voleurs à l'étage
		}
	}

	// nombre de cases scellées au tour précédent par tous les joueurs
	scanf("%d", &hotel->NS);

	// informations sur les cases scellées
	for(indiceNS = 0 ; indiceNS < hotel->NS ; indiceNS++){
		scanf("%d %d", &F, &C); // récupération des coordonnées de la chambre
		hotel->etages[F].cases[C].scelle = 1; // changement de l'état de la chambre
	}

	// nombre d'agents du service de surveillance dans un escalier (0 sinon)
	scanf("%d", &hotel->SURV);

	// nombre de voleurs par étage
	for(indiceSURV = 0 ; indiceSURV < hotel->SURV ; indiceSURV++){
		scanf("%d %d", &E, &tempNV);
		hotel->etages[E].NV = tempNV;
	}

	// nombre d'informations envoyées par la police scientifique
	scanf("%d", &NI);

	// localisation d'un voleur (F, C)
	for(indiceNI = 0 ; indiceNI < NI ; indiceNI++){
		scanf("%d %d", &F, &C);
		hotel->etages[F].cases[C].voleurSurPlace++;
	}
}

/* ****************************************************************************************** */
/* ******************************** Fonction envoiDesCommandes ****************************** */
/* ****************************************************************************************** */
/* Fonction qui envoie la chaîne de caractères contenant les commandes des agents, et libère l'espace mémoire alloué. */

/* Prend en entrée l'adresse de la chaîne de caractères à envoyer sur la sortie standard. */
void envoiDesCommandes(char* sortieStandard){
	// écriture de la commande sur la sortie standard
	printf("%s", sortieStandard);
	
	// libération de la mémoire allouée
	free(sortieStandard);
	sortieStandard = NULL;

	// force, si besoin, l'écriture de l'affichage
	fflush(stdout);
	fflush(stderr);
}

/* ****************************************************************************************** */
/* ****************************** Fonction analyseDesInformations *************************** */
/* ****************************************************************************************** */
/* Fonction qui analyse les informations reçues pour déterminer la stratégie adoptée pour gagner. */

/* Prend en entrée l'adresse de la structure hotel. */
/* Renvoie la sortie standard, NULL si erreur. */
char* analyseDesInformations(Hotel* hotel){
	char* sortieStandard = (char*) malloc(TAILLE_BUFFER * NOMBRE_AGENTS_PAR_JOUEUR * sizeof(char)); // chaine de caractères des actions des agents
	char* tempChar; // chaine de caractères de l'action d'un agent
	
	int indiceAgent; // indice de l'agent
	Etage* monEtage;
	Agent* monAgent;
	
	int deuxiemeTour = 1; // si le deuxième tour n'a pas été joué
	
	// boucle qui définit les actions de chacun de mes agents
	for(indiceAgent = hotel->agentsPerso[0] ; indiceAgent <= hotel->agentsPerso[NOMBRE_AGENTS_PAR_JOUEUR - 1] ; indiceAgent++){
		monAgent = &(hotel->agents[indiceAgent]);
		monEtage = &(hotel->etages[monAgent->F]);
		
		switch(monAgent->S){
			case EN_COURS_DE_SPECIALISATION :
				tempChar = (char*) malloc(TAILLE_BUFFER * sizeof(char));
				sprintf(tempChar, "PASS;"); // il ne peut rien faire pendant ce temps
				break;
				
			case BRIGADE_RAPIDE :
				if(hotel->compteurDeTours == 0){ // premier tour
					if(ETAGE_DEPART != 0){ // si la stratégie implique que les agents ne commencent pas au premier étage
						// les agents vont à l'étage de départ défini
						tempChar = actionsBrigadeRapideAccesDepart(hotel, monAgent);
					}
					
					else{
						// spécialisation d'un agent en service de surveillance
						tempChar = actionsBrigadeRapideSpecialisationSURV(monAgent);
						deuxiemeTour = 0;
					}
				}
				
				else if(deuxiemeTour && hotel->compteurDeTours == 1){ // deuxième tour, si les actions n'ont pas déjà été faites
					// spécialisation d'un agent en service de surveillance
					tempChar = actionsBrigadeRapideSpecialisationSURV(monAgent);
				}

				else{
					// si l'agent était spécialisé service de surveillance, et que les voleurs à l'étage ne sont pas les derniers encore en jeu
					if(monAgent->agentSURV && hotel->etages[monAgent->F].NV != hotel->nombreVoleursEnJeu){
						tempChar = actionsBrigadeRapideAgentSURV(hotel, monAgent);
					}
					
					else{
						tempChar = actionsBrigadeRapide(hotel, monEtage, monAgent);
					}
				}
				break;
				
			case PELOTON_DE_SURETE : // l'agent est spécialisé peloton de sûreté
				tempChar = actionsPelotonDeSurete(hotel, monEtage, monAgent);
				break;
				
			case SERVICE_DE_SURVEILLANCE : // l'agent est spécialisé service de surveillance	
				tempChar = actionsServiceDeSurveillance(hotel, monEtage, monAgent);
				break;
				
			case POLICE_SCIENTIFIQUE : // l'agent est spécialisé police scientifique
				tempChar = actionsPoliceScientifique();
				break;
				
			default: // action par défaut
				tempChar = (char*) malloc(TAILLE_BUFFER * sizeof(char));
				sprintf(tempChar, "PASS;");
				break;
		}
		strcat(sortieStandard, tempChar);
		free(tempChar);
		tempChar = NULL;
	}
	strcat(sortieStandard, "\n");
	return sortieStandard;
}

/* ****************************************************************************************** */
/* ********************************** Fonction finDePartie ********************************** */
/* ****************************************************************************************** */
/* Fonction qui libère l'espace mémoire alloué. */

/* Prend en paramètres le pointeur sur la structure Hotel. */
void finDePartie(Hotel* hotel){
	free(hotel->joueurs);
	free(hotel->agents);
	free(hotel);
}

/* ****************************************************************************************** */
/* ***************************** Fonctions d'action d'un agent ****************************** */
/* ****************************************************************************************** */
/* Fonction qui définit les actions d'un agent spécialisé brigade rapide au premier tour. */

/* Prend en paramètres le pointeur sur la structure Hotel, et le pointeur sur la structure de l'agent qui joue. */
/* Renvoie la commande pour cet agent. */
char* actionsBrigadeRapideAccesDepart(Hotel* hotel, Agent* monAgent){
	char* tempChar = (char*) malloc(TAILLE_BUFFER * sizeof(char));
	
	// l'agent se déplace à l'étage de départ défini par configuration statique
	monAgent->etageAViser = ETAGE_DEPART;
	
	if(testDeplacementAgent(monAgent->etageAViser, monAgent->C)){
		remiseAZeroFouilleEtage(&(hotel->etages[monAgent->etageAViser]));
		sprintf(tempChar, "MOVE %d %d %d;", monAgent->ID, monAgent->etageAViser, monAgent->C);
	}
	
	else{
		tempChar = "PASS;";
	}
	
	return tempChar;
}

/* Fonction qui définit les actions d'un agent spécialisé brigade rapide au deuxième tour. */
/* Si c'est l'agent dans l'escalier gauche, il se spécialise service de surveillance. */
/* Sinon, il attend. */

/* Prend en paramètres le pointeur sur la structure de l'agent qui joue. */
/* Renvoie la commande pour cet agent. */
char* actionsBrigadeRapideSpecialisationSURV(Agent* monAgent){
	char* tempChar = (char*) malloc(TAILLE_BUFFER * sizeof(char));

	if(monAgent->cote == COTE_GAUCHE){
		sprintf(tempChar, "SPEC %d %d;", monAgent->ID, SERVICE_DE_SURVEILLANCE);
	}
	
	else{
		sprintf(tempChar, "PASS;");
	}
	
	return tempChar;
}

/* Définit les actions d'un agent spécialisé brigade rapide. */
// Récupération des informations sur l'autre agent.
// S'il n'y a aucun voleur à l'étage : 
// 		Si l'agent est dans un escalier :
// 			Si les chambres sont scellées de manière à empêcher les voleurs de changer d'étage :
//				Il recherche un étage avec au moins un voleur, et se rend à cet étage.
//			Sinon :
// 				Il va au même étage que l'autre agent, spécialisé service de surveillance, afin de pouvoir fouiller l'étage dès qu'un voleur a été repéré.
// 		Sinon, s'il n'est pas dans un escalier :
// 			Il va dans l'escalier où il était au départ.
// Sinon, s'il y a au moins un voleur à l'étage : 
// 		Si la chambre où est l'agent a déjà été fouillée :
// 			Il cherche une chambre non scellée, qu'il n'a pas déjà fouillée, et qui n'a pas d'agent concurrent devant ou dedans.
// 			S'il en trouve une :
//				Il y va.
//			Sinon :
// 				Il recommence la fouille de l'étage à partir du début de l'étage.
// 		Sinon :
// 			Il la fouille.

/* Prend en paramètres le pointeur sur la structure Hotel, l'étage où se trouve l'agent, et le pointeur sur la structure de l'agent qui joue. */
/* Renvoie la commande pour cet agent. */
char* actionsBrigadeRapide(Hotel* hotel, Etage* monEtage, Agent* monAgent){
	char* tempChar = (char*) malloc(TAILLE_BUFFER * sizeof(char));
	
	Agent* monAutreAgent = &(hotel->agents[getIDAutreAgent(monAgent->ID)]);
	
	if(monEtage->NV == 0){ // s'il n'y a aucun voleur à l'étage
		if(testEscalier(monAgent->C)){ // si l'agent est dans un escalier
			if(hotel->etagesScelles){ // si les chambres sont scellées de manière à empêcher les voleurs de changer d'étage
				rechercheEtageAViser(hotel, monEtage, monAgent);
				
				if(testDeplacementAgent(monAgent->etageAViser, monAgent->C)){
					remiseAZeroFouilleEtage(&(hotel->etages[monAgent->etageAViser]));
					sprintf(tempChar, "MOVE %d %d %d;", monAgent->ID, monAgent->etageAViser, monAgent->C);
				}
				else{
					sprintf(tempChar, "PASS;");
				}
			}
			
			else{
				if(testDeplacementAgent(monAutreAgent->F, monAgent->C)){
					// l'agent brigade rapide va au même étage que l'autre agent pour, si besoin, fouiller l'étage
					remiseAZeroFouilleEtage(&(hotel->etages[monAgent->etageAViser]));
					sprintf(tempChar, "MOVE %d %d %d;", monAgent->ID, monAutreAgent->etageAViser, monAgent->C);
					remiseAZeroFouilleEtage(&(hotel->etages[monAutreAgent->F]));
				}
				else{
					sprintf(tempChar, "PASS;");
				}
			}
		}
		
		else{
			if(testDeplacementAgent(monAgent->F, monAgent->cote - monAgent->decal)){
				sprintf(tempChar, "MOVE %d %d %d;", monAgent->ID, monAgent->F, monAgent->cote - monAgent->decal);
			}
			
			else{
				sprintf(tempChar, "PASS;");
			}
		}
	}
	
	else{ // s'il y a au moins un voleur à l'étage
		if(monEtage->cases[monAgent->C].fouille){ // si la case a déjà été fouillée
			monAgent->caseAFouiller = testCaseAFouiller(monAgent, monEtage);
			
			if(testDeplacementAgent(monAgent->F, monAgent->caseAFouiller)){
				sprintf(tempChar, "MOVE %d %d %d;", monAgent->ID, monAgent->F, monAgent->caseAFouiller);
			}
			
			else{ // si toutes les chambres de l'étage ont été fouillées, alors l'agent recommence la fouille de l'étage
				remiseAZeroFouilleEtage(monEtage);
				monAgent->caseAFouiller = testCaseAFouiller(monAgent, monEtage);
				
				if(testDeplacementAgent(monAgent->F, monAgent->caseAFouiller)){
					sprintf(tempChar, "MOVE %d %d %d;", monAgent->ID, monAgent->F, monAgent->caseAFouiller);
				}
				
				else{
					sprintf(tempChar, "PASS;");
				}
			}
		}
		
		else{ // si la chambre n'a pas déjà été fouillé
			monEtage->cases[monAgent->C].fouille = 1;
			if(testRecherche(monAgent->C)){
				sprintf(tempChar, "SEARCH %d;", monAgent->ID);
			}
			
			else{
				if(testDeplacementAgent(monAgent->F, monAgent->C + monAgent->decal)){
					sprintf(tempChar, "MOVE %d %d %d;", monAgent->ID, monAgent->F, monAgent->C + monAgent->decal);
				}
				
				else{
					sprintf(tempChar, "PASS;");
				}
			}
		}
	}
	
	return tempChar;
}

/* La fonction définit les actions d'un agent spécialisé brigade rapide qui étaient spécialisés service de surveillance au tour d'avant. */
/* Lorsqu'il est arrivé en haut, et qu'il reste des voleurs dans l'hôtel, il redescend. */
// Si l'agent n'est pas à l'étage en-dessous de celui de départ :
// 		Il s'y rend.
// Sinon :
// 		Il se spécialise service de surveillance.

/* Prend en paramètres le pointeur sur la structure Hotel, et le pointeur sur la structure de l'agent qui joue. */
/* Renvoie la commande pour cet agent. */
char* actionsBrigadeRapideAgentSURV(Hotel* hotel, Agent* monAgent){
	char* tempChar = (char*) malloc(TAILLE_BUFFER * sizeof(char));
	
	monAgent->etageAViser = ETAGE_DEPART - 1;
	
	if(monAgent->etageAViser < 0){ // l'étage ne peut pas être négatif
		monAgent->etageAViser = 0;
	}
		
	if(monAgent->F != monAgent->etageAViser){ // si l'agent n'est pas au bon étage
		sprintf(tempChar, "MOVE %d %d %d;", monAgent->ID, monAgent->etageAViser, monAgent->C);
		remiseAZeroFouilleEtage(&(hotel->etages[monAgent->etageAViser]));
	}
		
	else{ // si l'agent est au bon étage, il se (re)spécialise en service de surveillance
		sprintf(tempChar, "SPEC %d %d;", monAgent->ID, SERVICE_DE_SURVEILLANCE);
	}
	
	return tempChar;
}

/* La fonction définit les actions d'un agent spécialisé peloton de sûreté. */
/* L'agent scelle la chambre à côté de l'escalier d'un étage sur deux, pour empêcher les voleurs de changer d'étage. */
/* Le scellement des chambres, même d'un étage sur deux, était trop lent, par rapport aux concurrents. Cette fonction n'est donc pas utilisée dans la stratégie actuelle. */

/* Prend en paramètres le pointeur sur la structure Hotel, l'étage où se trouve l'agent, et le pointeur sur la structure de l'agent qui joue. */
/* Renvoie la commande pour cet agent. */
char* actionsPelotonDeSurete(Hotel* hotel, Etage* monEtage, Agent* monAgent){
	char* tempChar = (char*) malloc(TAILLE_BUFFER * sizeof(char));
	int testCaseScellee = 0;
	
	testCaseScellee = testEtagesScelles(hotel);
	
	if(testCaseScellee){ // si les cases extrêmes d'un étage sur deux sont scellées
		if(testRecherche(monAgent->C)){ // si l'agent est pas dans un escalier
			if(testDeplacementAgent(monAgent->F, monAgent->C - monAgent->decal)){
				sprintf(tempChar, "MOVE %d %d %d;", monAgent->ID, monAgent->F, monAgent->C - monAgent->decal);
			}
			
			else{
				sprintf(tempChar, "PASS;");
			}
		}
		
		else{ // s'il est dans un escalier, il se spécialise soit en service de surveillance, soit en brigade rapide
			int agent;
			if(monAgent->cote == COTE_GAUCHE){
				agent = SERVICE_DE_SURVEILLANCE;
			}
			
			else{
				agent = BRIGADE_RAPIDE;
			}
			
			sprintf(tempChar, "SPEC %d %d;", monAgent->ID, agent);
		}
	}
		
	else{
		if(!monEtage->cases[monAgent->cote].scelle && (monAgent->F % 2 == 0)){ // si la case n'est pas scellé et si l'agent est à un étage pair (il ne scelle qu'un étage sur deux)
			if(testEscalier(monAgent->C)){ // si l'agent est dans un escalier
				if(testDeplacementAgent(monAgent->F, monAgent->C + monAgent->decal)){
					sprintf(tempChar, "MOVE %d %d %d;", monAgent->ID, monAgent->F, monAgent->C + monAgent->decal);
				}
				
				else{
					sprintf(tempChar, "PASS;");
				}
			}
			
			else{
				if(testRecherche(monAgent->C)){
					sprintf(tempChar, "SEARCH %d;", monAgent->ID);
					monEtage->cases[monAgent->C].fouille = 1;
				}
				
				else{
					sprintf(tempChar, "PASS;");
				}
			}
		}
		else{
			if(monAgent->C == monAgent->cote){
				if(testDeplacementAgent(monAgent->F, monAgent->C - monAgent->decal)){
					sprintf(tempChar, "MOVE %d %d %d;", monAgent->ID, monAgent->F, monAgent->C - monAgent->decal);
				}
				
				else{
					sprintf(tempChar, "PASS;");
				}
			}
			else{
				if(monAgent->F == NOMBRE_ETAGES){
					monAgent->montee = -1;
				}
				
				monAgent->etageAViser = monAgent->F + monAgent->montee;
				
				if(testDeplacementAgent(monAgent->etageAViser, monAgent->C)){
					remiseAZeroFouilleEtage(&(hotel->etages[monAgent->etageAViser]));
					sprintf(tempChar, "MOVE %d %d %d;", monAgent->ID, monAgent->etageAViser, monAgent->C);
				}
				
				else{
					sprintf(tempChar, "PASS;");
				}
			}
		}
	}
	return tempChar;
}

/* La fonction définit les actions d'un agent spécialisé service de surveillance. */
/* Il se déplace d'étage en étage pour recenser les voleurs à chaque étage. */
// Si la chambre à côté des escaliers d'un étage sur deux sont scellées (donc que les voleurs ne peuvent plus changer d'étage) :
// 		Il recense le nombre de voleurs repérés.
//		S'il a trouvé le dernier étage avec des voleurs : il fouille une chambre par une chambre.
// 		S'il a parcouru tous les étages ou qu'il a repéré tous les voleurs : il se spécialise brigade rapide pour fouiller plus rapidement les étages.
// 		Sinon : il continue de parcourir les étages jusqu'à avoir repéré tous les voleurs.
// Sinon :
// 		S'il y a au moins un voleur à l'étage :
//			Si les voleurs à l'étage ne sont pas les derniers en jeu :
// 				Il attend que l'agent spécialisé brigade rapide les trouve.
// 			Sinon :
// 				Il fouille une chambre par une chambre.
// 		Sinon :
// 			Il se déplace à l'étage au-dessus, jusqu'à atteindre le dernier, auquel cas, il revient à celui en-dessous de celui de départ.

/* Prend en paramètres le pointeur sur la structure Hotel, l'étage où se trouve l'agent, et le pointeur sur la structure de l'agent qui joue. */
/* Renvoie la commande pour cet agent. */
char* actionsServiceDeSurveillance(Hotel* hotel, Etage* monEtage, Agent* monAgent){
	char* tempChar = (char*) malloc(TAILLE_BUFFER * sizeof(char));
	monEtage->verifFinie = 1;
	
	int etage;
	
	// Test pour savoir si les chambres près des escaliers d'au moins un étage sur deux sont scellées
	int testCaseScellee = 0;
	if(!(hotel->etagesScelles)){
		testCaseScellee = testEtagesScelles(hotel);
	}
	
	if(testCaseScellee || hotel->etagesScelles){
		hotel->etagesScelles = 1;
		
		int sommeVoleurs = 0;
		
		for(etage = 0 ; etage < NOMBRE_ETAGES ; etage++){
			sommeVoleurs +=  hotel->etages[etage].NV;
		}
		
		if(hotel->etages[monAgent->F].NV == hotel->nombreVoleursEnJeu){ // si c'est le dernier étage avec des voleurs
			tempChar = deplacementServiceDeSurveillanceDernierEtage(monEtage, monAgent);
		}
		
		else if(hotel->verifTotaleFinie || hotel->nombreVoleursEnJeu == sommeVoleurs){ // si tous les étages avec des voleurs ont été repéré ou que tous les étages ont été parcouru avec le service de surveillance
			sprintf(tempChar, "SPEC %d %d;", monAgent->ID, BRIGADE_RAPIDE);
		}
		
		else{ // si des voleurs n'ont pas été repéré
			tempChar = deplacementServiceDeSurveillance(hotel, monAgent);
		}
	}
	
	else{
		if(hotel->etages[monAgent->F].NV != 0){ // s'il y a au moins un voleur à l'étage
			hotel->verifTotaleFinie = 1;
			for(etage = 0 ; etage < NOMBRE_ETAGES ; etage++){
				if(hotel->etages[etage].verifFinie == 0){
					hotel->verifTotaleFinie = 0;
					break;
				}
			}
			
			if(hotel->etages[monAgent->F].NV != hotel->nombreVoleursEnJeu){ // si les voleurs à l'étage ne sont pas les derniers encore en jeu
				sprintf(tempChar, "PASS;");
			}
				
			else{ // si tous les voleurs non capturés sont à cet étage, l'agent fouille les chambres de son côté
				tempChar = deplacementServiceDeSurveillanceDernierEtage(monEtage, monAgent);
			}
		}
		
		else{ // s'il n'y a pas de voleur à l'étage
			if(monAgent->agentSURV){ // si c'est un nouvel agent spécialisé service de surveillance
				monAgent->montee = -1;
				monAgent->agentSURV = 0;
			}
			
			else{
				if(monAgent->F == NOMBRE_ETAGES - 1){
					monAgent->montee = -1;
					sprintf(tempChar, "SPEC %d %d;", monAgent->ID, BRIGADE_RAPIDE); // pour aller rapidement à l'étage juste en-dessous de celui de départ
					monAgent->agentSURV = 1;
					return tempChar;
				}
					
				else{
					monAgent->montee = 1;
				}
			}
			
			monAgent->etageAViser = monAgent->F + monAgent->montee; // mise à jour de l'étage de destination
			
			if(testDeplacementAgent(monAgent->etageAViser, monAgent->C)){
				remiseAZeroFouilleEtage(&(hotel->etages[monAgent->etageAViser]));
				sprintf(tempChar, "MOVE %d %d %d;", monAgent->ID, monAgent->etageAViser, monAgent->C);
			}
			
			else{
				sprintf(tempChar, "PASS;");
			}
		}
	}
	
	return tempChar;
}

/* La fonction définit les actions d'un agent spécialisé service de surveillance qui parcourt tous les étages pour trouver tous les voleurs encore en jeu. */
/* Tant qu'il a pas trouvé tous les voleurs, il parcourt les étages de l'étage de départ à l'étage le plus haut, puis les étages en-dessous l'étage de départ jusqu'à l'étage le plus bas, puis remonte. */

/* Prend en paramètres le pointeur sur la structure Hotel, l'étage où se trouve l'agent, et le pointeur sur la structure de l'agent qui joue. */
/* Renvoie la commande pour cet agent. */
char* deplacementServiceDeSurveillance(Hotel* hotel, Agent* monAgent){
	char* tempChar = (char*) malloc(TAILLE_BUFFER * sizeof(char));
	
	if(monAgent->agentSURV){ // si c'est un nouvel agent spécialisé service de surveillance
		monAgent->montee = -1;
		monAgent->agentSURV = 0;
	}
	
	else{
		if(monAgent->F == NOMBRE_ETAGES - 1){
			monAgent->montee = -1;
			sprintf(tempChar, "SPEC %d %d;", monAgent->ID, BRIGADE_RAPIDE); // pour aller rapidement à l'étage juste en-dessous de celui de départ
			monAgent->agentSURV = 1;
			return tempChar;
		}
			
		else{
			monAgent->montee = 1;
		}
	}
	
	monAgent->etageAViser = monAgent->F + monAgent->montee; // pour que l'autre agent puisse avoir l'information un tour avant
	
	if(testDeplacementAgent(monAgent->F, monAgent->C)){
		remiseAZeroFouilleEtage(&(hotel->etages[monAgent->etageAViser]));
		sprintf(tempChar, "MOVE %d %d %d;", monAgent->ID, monAgent->etageAViser, monAgent->C);
	}
	
	else{
		sprintf(tempChar, "PASS;");
	}
	
	return tempChar;
}

/* La fonction définit les actions d'un agent spécialisé service de surveillance lorsqu'il a trouvé le dernier étage avec des voleurs. */
/* Il fouille chaque chambre, sauf les chambres scellées. */

/* Prend en paramètres l'étage où se trouve l'agent, et le pointeur sur la structure de l'agent qui joue. */
/* Renvoie la commande pour cet agent. */
char* deplacementServiceDeSurveillanceDernierEtage(Etage* monEtage, Agent* monAgent){
	char* tempChar = (char*) malloc(TAILLE_BUFFER * sizeof(char));
	
	if(monEtage->cases[monAgent->C].scelle){ // si la chambre est scellée
		if(testDeplacementAgent(monAgent->F, monAgent->C + monAgent->decal)){
			sprintf(tempChar, "MOVE %d %d %d;", monAgent->ID, monAgent->F, monAgent->C + monAgent->decal);
			monEtage->cases[monAgent->C + monAgent->decal].fouille = 1;
		}
		
		else{
			sprintf(tempChar, "PASS;");
		}
	}
	
	else if(!monEtage->cases[monAgent->C].fouille){ // si la chambre n'a pas été fouillé par un des agents du joueur
		if(testRecherche(monAgent->C)){
			sprintf(tempChar, "SEARCH %d;", monAgent->ID);
			monEtage->cases[monAgent->C].fouille = 1;
		}
		
		else{
			sprintf(tempChar, "PASS;");
		}
	}
	
	else{ // si la chambre a déjà été fouillé et qu'elle n'est pas scellée
		if(testDeplacementAgent(monAgent->F, monAgent->C + monAgent->decal)){
			sprintf(tempChar, "MOVE %d %d %d;", monAgent->ID, monAgent->F, monAgent->C + monAgent->decal);
		}
		
		else{
			sprintf(tempChar, "PASS;");
		}
	}
	
	return tempChar;
}

/* La fonction définit les actions d'un agent spécialisé police scientifique. */
/* Spécification inutilisée dans la stratégie car la recherche d'indice prend trop de tours. */

/* Renvoie la commande pour cet agent. */
char* actionsPoliceScientifique(){
	return "PASS;";
}

/* ****************************************************************************************** */
/* ******************************** Fonctions de bas niveau ********************************* */
/* ****************************************************************************************** */

/* Vérifie que le joueur ne sort pas de l'hôtel. */

/* Prend en paramètres l'étage et la colonne. */
/* Renvoie 1 si c'est dans l'hôtel, 0 sinon. */
int testDeplacementAgent(int F, int C){
	if(C >= 0 && C < NOMBRE_COLONNES && F >= 0 && F < NOMBRE_ETAGES){
		return 1;
	}
	
	else{
		fprintf(stderr, "Déplacement incorrect : la case (%d, %d) est située hors de l'hôtel.\r\n", F, C);
		return 0;
	}
}

/* Vérifie si la case à fouiller est bien une chambre. */

/* Prend en paramètres la colonne. */
/* Renvoie 1 si c'est une chambre, 0 si c'est un escalier. */
int testRecherche(int C){
	if(C > 0 && C < (NOMBRE_COLONNES - 1)){
		return 1;
	}
	
	else{
		fprintf(stderr, "Recherche incorrecte : la colonne %d est un escalier.\r\n", C);
		return 0;
	}
}

/* Vérifie si la case à fouiller est un escalier. */
/* La fonction utilise la fonction testRecherche, qui vérifie que la case est une chambre. */

/* Prend en paramètres la colonne. */
/* Renvoie le contraire de la fonction testRecherche : 1 si c'est un escalier, 0 si c'est une chambre. */
int testEscalier(int C){
	if(C > 0 && C < (NOMBRE_COLONNES - 1)){
		return 0;
	}
	
	else{
		return 1;
	}
}

/* Vérifie si les chambres à côté des escaliers d'un étage sur deux sont scellées. */
/* Ainsi, les voleurs ne peuvent plus changer d'étage. */

/* Prend en paramètres la structure de jeu Hotel. */
/* Renvoie 1 si les chambres à côté des escaliers d'un étage sur deux sont scellées, 0 sinon. */
int testEtagesScelles(Hotel* hotel){
	int etagePair;
	int test = 1;
		
	for(etagePair = 0 ; etagePair < NOMBRE_ETAGES ; etagePair++){
		if(etagePair % 2 == 0){
			continue;
		}
		
		if(!hotel->etages[etagePair].cases[COTE_GAUCHE].scelle || !hotel->etages[etagePair].cases[COTE_DROIT].scelle){ // si une chambre à côté des escaliers pour les étages pairs n'est pas scellée, faire le test pour les étages impairs
			int etageImpair;
			for(etageImpair = 0 ; etageImpair < NOMBRE_ETAGES ; etageImpair++){
				if(etageImpair % 2 == 1){
					continue;
				}
				
				if(!hotel->etages[etageImpair].cases[COTE_GAUCHE].scelle || !hotel->etages[etageImpair].cases[COTE_DROIT].scelle){// si une chambre à côté des escaliers pour les étages impairs n'est pas scellée
					test = 0;
					return test;
				}
			}
			break;
		}
	}
	
	return test;
}

/* La fonction cherche un étage avec au moins un voleur. */
/* Si ce n'est pas le dernier étage avec au moins un voleur, l'agent ne va pas sur le même étage qu'un autre agent du même joueur. */

/* Prend en paramètres le pointeur sur la structure Hotel, l'étage où se trouve l'agent, et le pointeur sur la structure de l'agent qui joue. */
void rechercheEtageAViser(Hotel* hotel, Etage* monEtage, Agent* monAgent){
	int etage;
	
	remiseAZeroFouilleEtage(monEtage);
	
	Agent* monAutreAgent = &(hotel->agents[getIDAutreAgent(monAgent->ID)]);
	
	monAgent->etageAViser = monAutreAgent->F;
	
	// parcours des étages afin de trouver un étage avec au moins un voleur
	for(etage = 0 ; etage < NOMBRE_ETAGES ; etage++){
		if(hotel->etages[etage].NV != 0){
			if(hotel->etages[monAgent->F].NV != hotel->nombreVoleursEnJeu && monAutreAgent->etageAViser == etage){ // l'agent ne vise pas le même étage que l'autre agent si tous les voleurs ne sont pas à cet étage
				continue;
			}
			
			monAgent->etageAViser = etage;
			break;
		}
	}
}

/* La fonction remet à 0 l'état de fouille des chambres d'un étage. */

/* Prend en paramètres un étage. */
void remiseAZeroFouilleEtage(Etage* monEtage){
	int colonne;
	for(colonne = 1 ; colonne < NOMBRE_COLONNES - 1 ; colonne++){ // remise à 0 des états de fouille des chambres de l'étage
		monEtage->cases[colonne].fouille = 0;
	}
}

/* La fonction cherche la chambre la plus proche de l'agent, qui soit à la fois non scellée et non fouillée par un des agents du joueur. */

/* Prend en paramètres l'agent en question du joueur, ainsi que l'étage à fouiller */
/* Renvoie la chambre à fouiller si elle existe, sinon une case hors du plateau (ici le nombre de colonnes) */
int testCaseAFouiller(Agent* monAgent, Etage* monEtage){
	int caseAFouiller;
	for(caseAFouiller = monAgent->C - 1 ; caseAFouiller > 0 && caseAFouiller < NOMBRE_COLONNES - 1 ; caseAFouiller += monAgent->decal){
		if(monEtage->cases[caseAFouiller].scelle){ // si la chambre est scellée
			monEtage->cases[caseAFouiller].fouille = 1;
			continue;
		}
		
		if(monEtage->cases[caseAFouiller].agentSurPlace){ // si un agent concurrent est devant ou dans la chambre
			monEtage->cases[caseAFouiller].fouille = 1;
			continue;
		}
		
		if(!monEtage->cases[caseAFouiller].fouille){ // si la chambre n'a pas été fouillé par un des agents du joueur
			return caseAFouiller;
		}
	}
	
	return NOMBRE_COLONNES;
}

/* La fonction renvoie l'ID de l'autre agent du joueur. */
/* La stratégie est basée sur une recherche lorsqu'il y deux agents par joueur. Cependant, les données sont provisionnées pour un nombre variable d'agents. */

/* Prend en paramètres l'ID de l'agent. */
/* Renvoie l'ID de l'autre agent. */
int getIDAutreAgent(int ID){
	// Récupération des informations sur l'autre agent
	if(ID % NOMBRE_AGENTS_PAR_JOUEUR == 0){
		return ID + 1;
	}
	
	else{
		return ID - 1;
	}
}
