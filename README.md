# CatchTheThief
Ce jeu consiste en la traque de cambrioleurs dans un hôtel. Vous contrôlez une agence des forces de l'ordre et devez mettre la main sur ces cambrioleurs avant les autres agences.

CatchTheThief
par Dimitri Watel

Requis: java8, php, un navigateur acceptant d'exécuter du javascript (optionnel : python3 pour pouvoir lancer une des IA codée en python3)

Lancement :
- extraire tous les fichiers dans un dossier (pour l'exemple, on l'appelera catchthethief)
- aller dans le dossier catchthethief

Pour avoir de l'aide:
- java -jar catchthethief.jar -h

Pour tester
- java -jar catchthethief.jar -b1 -b2 
- cd html
- php -S localhost:8888 &
- firefox index.php
