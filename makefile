CC = gcc -Wall -Wextra -Wrestrict -std=c99 -lm

all : ia

ia : ia.o
	$(CC) $^ -o $@
		
%.o : %.c
	$(CC) -c $^

clean :
	rm *.o
